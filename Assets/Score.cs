﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Score : MonoBehaviour
{
    static public Score Instance { get; private set; }
    private void Awake()
    {
        if (Instance == null) Instance = this;
        else Destroy(Instance.gameObject);

        if (PlayerPrefs.HasKey("HighScore"))
        {
            highScore = PlayerPrefs.GetInt("HighScore");
        }

    }
    [SerializeField]TextMeshProUGUI bestScoreText;
    [SerializeField]TextMeshProUGUI scoreText;

    int score, highScore;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        bestScoreText.text = "Best Score: " + PlayerPrefs.GetInt("HighScore");
        scoreText.text = "Score: " + score.ToString();
    }

    internal void UpScore()
    {
        score += 100;
        if (score > PlayerPrefs.GetInt("HighScore")) PlayerPrefs.SetInt("HighScore", score);
    }
}
