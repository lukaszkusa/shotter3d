﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMove : MonoBehaviour
{

    public GameObject[] panels;
    public float scrollSpeed = 0.5f;
    Vector3 temPos;
    float backgroundHight;
    float startTime;
    int index;
    // Start is called before the first frame update
    void Start()
    {
        transform.position = Vector3.zero;
        backgroundHight = (panels[0].transform.localScale.y / 2) - CameraBorder.Instance.camHeight;
        startTime = Time.time;
        temPos = panels[0].transform.position;
        index = 0;
    }

    // Update is called once per frame
    void Update()
    {
       
        if (temPos.y > backgroundHight)
        {
            if (index == 0) index++;
            else index--;
            startTime = Time.time;
        }

        temPos = panels[index].transform.position;
        temPos.y = (Time.time - startTime) * scrollSpeed;
        panels[index].transform.position = temPos;
    }
}
