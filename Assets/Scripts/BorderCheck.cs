﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorderCheck : MonoBehaviour
{
    public float offset = 2f;
    public bool keepOnScreen = true;
    public bool isOnScrean = true;
    float height, width;
    // Start is called before the first frame update
    void Start()
    {
        height = CameraBorder.Instance.camHeight;
        width = CameraBorder.Instance.camWidth;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 tempPos = transform.position;

        if (tempPos.x > width - offset)
        {
            isOnScrean = false;
            tempPos.x = width - offset;
        }
        if (tempPos.x < -width + offset)
        {
            tempPos.x = -width + offset;
            isOnScrean = false;
        }

        if (tempPos.y > height - offset)
        {
            tempPos.y = height - offset;
            isOnScrean = false;
        }
        if (tempPos.y < -height + offset)
        {
            tempPos.y = -height + offset;
            isOnScrean = false;
        }

        transform.position = tempPos;

        if (!isOnScrean && !keepOnScreen) Destroy(this.gameObject);
        
    }

    private void OnDrawGizmos()
    {
        if (Application.isPlaying)
        {
            Vector3 boundSize = new Vector3(width * 2, height * 2, 0.1f);
            Gizmos.DrawWireCube(Vector3.zero, boundSize);
        }
    }
}
