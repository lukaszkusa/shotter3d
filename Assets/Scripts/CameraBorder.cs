﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraBorder : MonoBehaviour
{

    public static CameraBorder Instance { get; private set; }

    public float camWidth, camHeight;

    private bool first = false;
    private void Awake()
    {
       
        if (Instance == null) Instance = this;
        else Destroy(this.gameObject);
        camHeight = Camera.main.orthographicSize;
        camWidth = camHeight * Camera.main.aspect;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
       
    }
}
