﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public float speed = 10f;
    protected BorderCheck borderCheck;
    protected float camHeight, camWidth;
    protected float offset;

    protected Vector3 pos
    {
        get{ return transform.position; }
        set{ transform.position = value; }

    }
    // Start is called before the first frame update
    void Awake()
    {
        borderCheck = GetComponent<BorderCheck>();
        offset = borderCheck.offset;
        camHeight = CameraBorder.Instance.camHeight;
        camWidth = CameraBorder.Instance.camWidth;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    protected virtual void Move()
    {
        Vector3 pos = transform.position;
        pos.y -= speed * Time.deltaTime;

        transform.position = pos;
    }
}
