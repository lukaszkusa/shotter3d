﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2 : Enemy
{
    Vector3 p0, p1;
    float timeStart;
    float frequecny = 2;
    private void Start()
    {
        p0 = p1 = pos;
        timeStart = Time.time;
        GeneratePoint();
    }

    protected override void Move()
    {
        float u = (Time.time - timeStart) / frequecny;
        if(u >= 1)
        {
            GeneratePoint();
            u = 0;
        }

        u = 1 - Mathf.Pow(1 - u, 2);
        pos = (1 - u) * p0 + u * p1;
        
    }
    void GeneratePoint()
    {
        p0 = p1;

        p1.x = Random.Range(-camWidth + offset, camWidth - offset);
        p1.y = Random.Range(-camHeight + offset, camHeight - offset);
        timeStart = Time.time;

    }

}
