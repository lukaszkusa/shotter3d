﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{
    static public EnemyGenerator Instance;

    public Enemy [] enemyPrefab;
    public float frequency;
    // Start is called before the first frame update
    void Awake()
    {
        if (Instance == null) Instance = this;
        else Destroy(this.gameObject);
        Invoke("Generate", 2);
     
    }
   
    // Update is called once per frame
    void Update()
    {
        
    }


    void Generate()
    {
        Enemy enemy = Instantiate<Enemy>(enemyPrefab[Random.Range(0, enemyPrefab.Length)]);
        float offset = enemy.GetComponent<BorderCheck>().offset;
        float x = Random.Range(-CameraBorder.Instance.camWidth + offset, CameraBorder.Instance.camWidth - offset);
        float y = CameraBorder.Instance.camHeight-offset;
        enemy.transform.position = new Vector3(x, y, 0);
        Invoke("Generate", 1/frequency);
    }
}
