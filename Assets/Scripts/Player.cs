﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public static Player Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null) Instance = this;
        else Destroy(this.gameObject);

        
    }

    public GameObject projectilePrefab;
    public int shieldLevel = 4;
    public float speedFactor = 10;
    public float rotateFactor = 40f;
    float vAxis, hAxis;
    Vector3 posTemp;
    AudioSource audio;
    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            TempFire();
            audio.Play();
        }

        if (shieldLevel == -1)SceneManager.LoadScene(0);
    }
    void FixedUpdate()
    {
        posTemp = transform.position;
        vAxis = Input.GetAxis("Vertical");
        hAxis = Input.GetAxis("Horizontal");
        posTemp.x += hAxis * speedFactor;
        posTemp.y += vAxis * speedFactor;

        transform.position = posTemp;
        float y = rotateFactor * hAxis;
        transform.rotation = Quaternion.Euler(-90, y, 0);
    }
    void TempFire()
    {
        GameObject projGO = Instantiate<GameObject>(projectilePrefab);
        projGO.transform.position = transform.position;
        Rigidbody rigidB = projGO.GetComponent<Rigidbody>();
        rigidB.velocity = Vector3.up * speedFactor;
    }
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.tag == "Enemy") shieldLevel = Mathf.Clamp(--shieldLevel, -1, 4);
    }
}
