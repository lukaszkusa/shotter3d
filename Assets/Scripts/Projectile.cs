﻿      using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float speedFactor = 2f;
   public  Rigidbody projectileRigiBody;
    // Start is called before the first frame update
    void Awake()
    {
        projectileRigiBody = GetComponent<Rigidbody>();     
    }

    private void Start()
    { 
    }

    // Update is called once per frame
    void Update()
    {
        projectileRigiBody.velocity = Vector3.up * speedFactor;
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy")
        {
            Score.Instance.UpScore();
            Destroy(other.gameObject);
            Destroy(this.gameObject);
        }
    }
}
