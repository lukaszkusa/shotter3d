﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    public float rotateFactor = 2f;
    public float levelShown = 0;
    Material mat;

    // Start is called before the first frame update
    void Start()
    {
        mat = GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        if(levelShown != Player.Instance.shieldLevel)
        {
            levelShown = Player.Instance.shieldLevel;
            mat.mainTextureOffset = new Vector2(0.2f * levelShown, 0);

        }

        float z = -(rotateFactor * Time.time * 360) % 360;
        transform.rotation = Quaternion.Euler(0, 0, z);
    }
}
